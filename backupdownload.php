<?php
// Example. Zip all .html files in the current directory and save to current directory.
// Make a copy, also to the current dir, for good measure.
//$mem = ini_get('memory_limit');
//$extime = ini_get('max_execution_time');
//
////ini_set('memory_limit', '512M');
//ini_set('max_execution_time', 120);

include_once("ZipStream.php");
//print_r(ini_get_all());
// $files = glob('/home/minecraft/backups/worlds/world/*');
// $files = array_combine(array_map('filectime', $files),$files);
// arsort($files);
// $lastworld=array_shift(array_values($files));
// $files = glob('/home/minecraft/backups/worlds/DIM100/*');
// $files = array_combine(array_map('filectime', $files),$files);
// arsort($files);
// $lastDIM100=array_shift(array_values($files));
// $files = glob('/home/minecraft/backups/worlds/DIM-17/*');
// $files = array_combine(array_map('filectime', $files),$files);
// arsort($files);
// $lastDIM17=array_shift(array_values($files));
// $files = glob('/home/minecraft/backups/worlds/DIM-127/*');
// $files = array_combine(array_map('filectime', $files),$files);
// arsort($files);
// $lastDIM127=array_shift(array_values($files));
// $files = glob('/home/minecraft/backups/worlds/DIM-1/*');
// $files = array_combine(array_map('filectime', $files),$files);
// arsort($files);
// $lastDIM1=array_shift(array_values($files));
				
$fileTime = date("D, d M Y H:i:s T");

$zip = new ZipStream("backupMapa-". date('Y-m-d--H-i-s') .".zip");

$zip->setComment("Backup Mapa\nCreat el " . date('l jS \of F Y h:i:s A'));
$zip->addDirectoryContent("/home/minecraft/world", "world");
// $zip->addLargeFile($lastworld,'world.zip');
// $zip->addLargeFile($lastDIM100,'DIM100.zip');
// $zip->addLargeFile($lastDIM17,'DIM-17.zip');
// $zip->addLargeFile($lastDIM127,'DIM-127.zip');
// $zip->addLargeFile($lastDIM1,'DIM-1.zip');
$zip->finalize(); // Mandatory, needed to send the Zip file directory structure.
?>